<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'sadeen' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '/[%5jnEt[iIg(cq>CA;+lp._4fD3ToP,);DZZ;^]+vru%((8oMAMQCgFMD`1&7W%' );
define( 'SECURE_AUTH_KEY',  'sGoK(S4#%)[|)Sui0~fkX{/5}asB:@1.P8dadCcO)Yrm:2uI:6&B!7,NB<$jt;K@' );
define( 'LOGGED_IN_KEY',    ']H9AhNm8$Ms(y;T TeVD8!lPe?>o;*tjJ-oy{_[;i0dBo:U}7JIv*.1K,3<W]0iN' );
define( 'NONCE_KEY',        'c1h=L~/)Ri`g.3.0y..*qn=RQdM>|c-K+m{~E_ TvwSS_SIpbxhxDpDr;y+kj;)4' );
define( 'AUTH_SALT',        'O%Lt5jSi8;&^HR0MF)2wr/23tDab_ii0[xW86[5wA6{U_(>4}DEVT{+(K{u(+3W4' );
define( 'SECURE_AUTH_SALT', 's8D7en0mbu$O lgrhhvOUN-N%;mY H`OgnmkIQCqM9`bmQ+@^hYB`S>3*&5HWU4 ' );
define( 'LOGGED_IN_SALT',   '8P6 g[nq|8QKt.WzB*>iBc5iOkeiPHw?wq9!3<e7pOc<R=)(SyX*Ub:9* vf_Mqf' );
define( 'NONCE_SALT',       'v|/G3%mxTM~V{][E=[pa927^n(.9Jm(|Zz.UgmGDVV5H6iO!HRG7Q816=!LarB$v' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
