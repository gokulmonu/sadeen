<style type="text/css">
    .single-product form.cart input {
         width: 8em !important;
    }
</style>

<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @package    WooCommerce/Templates
 * @version     1.6.4
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

get_header('shop'); ?>


<?php

$post_id = get_the_ID();


$image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'single-post-thumbnail');

$product = new WC_product($post_id);
$attachment_ids = $product->get_gallery_image_ids();
foreach ($attachment_ids as $attachment_id) {
    $image_url = wp_get_attachment_url($attachment_id);
}

$title = $product->get_title();

$description = $product->get_description();

$price = $product->get_price_html();
$regular_price = (float) $product->get_regular_price();
$sale_price = (float) $product->get_price();
$precision = 1;
$saving_percentage = round( 100 - ( $sale_price / $regular_price * 100 ), 1 ) . '%';

$reviewCount = $product->get_review_count('view');

$args = array('post_id' => $post_id);
$comments = get_comments($args);

$add_to_cart = do_shortcode('[add_to_cart_url id="' . $post_id . '"]');
?>
<?php
$args = array(
    'delimiter' => ' | '
);
?>
<section class="bread">
    <div class="container-fluid">
        <ul class="bread-crumb">
            <?php woocommerce_breadcrumb( $args ); ?>
        </ul>
    </div>
</section>
<?php wc_print_notices(); ?>
    <section class="product-display">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 col-lg-6">
                    <div class="product-list">
                        <div class="percentage">
                            <?= $saving_percentage ?> <br> Off
                        </div>
                        <div class="row">
                            <div class="col-3">
                                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                                     aria-orientation="vertical">
                                    <?php $imgIndex = 1; ?>
                                    <?php foreach ($attachment_ids as $imgId): ?>
                                        <a class="nav-link <?php if ($imgIndex == 1): ?>active<?php endif; ?>"
                                           id="v-pills-image-tab-<?= $imgIndex ?>" data-toggle="pill"
                                           href="#v-pills-image<?= $imgIndex ?>" role="tab"
                                           aria-controls="v-pills-image<?= $imgIndex ?>"
                                           aria-selected="<?php if ($imgIndex == 1): ?>true<?php endif; ?>">
                                            <div class="product-thumb">
                                                <?php
                                                $image_url = wp_get_attachment_url($imgId);
                                                ?>
                                                <img src="<?= $image_url ?>" alt="">
                                            </div>
                                        </a>
                                        <?php $imgIndex++; ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="col-9">

                                <div class="tab-content" id="v-pills-tabContent">
                                    <?php $imgIndex = 1; ?>
                                    <?php foreach ($attachment_ids as $imgId): ?>
                                        <div
                                            class="tab-pane fade show <?php if ($imgIndex == 1): ?>active<?php endif; ?>"
                                            id="v-pills-image<?= $imgIndex ?>" role="tabpanel"
                                            aria-labelledby="v-pills-image<?= $imgIndex ?>-tab">

                                            <div class="product-active">
                                                <?php
                                                $image_url = wp_get_attachment_url($imgId);
                                                ?>
                                                <img src="<?= $image_url ?>" alt="">
                                            </div>

                                        </div>
                                        <?php $imgIndex++; ?>
                                    <?php endforeach; ?>

                                </div>
                            </div>
                        </div>

                        <ul class="wish-list">
                            <li>
                                
                                <?= do_shortcode('[ti_wishlists_addtowishlist product_id="' . $post_id . '"]');?>
                                <span style="margin-left: 52px;">
                                       <?= do_shortcode('[Sassy_Social_Share]');?>
                                </span>
                            </li>
                            <li>

<!--                                    --><?php //get_star_rating(); ?>
                                    <div class="rating-custom">
                                        <?php wc_get_template( 'single-product/rating.php' ); ?>
                                    </div>

<!--                                <span> 0 reviews / Write a review</span>-->
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-6">
                    <ul class="product-detail">
                        <h2 class="product-name">
                            <?= $title ?>
                        </h2>
                        <!--						<div class="p-subtitle">-->
                        <!--							<span class="title-highlight">Product Code:</span> <span> KIMBO Capsule automatic machine</span>-->
                        <!--						</div>-->
                        <ul class="price-details">
                            <li>
                                <div class="price-main"> <?= $price ?>
                                    <span class="stock"> In Stock</span>
                                </div>
                            </li>
                            <li class="sbmt">
                            <div class="qty qty1">
                                    Qty
                                </div>
                                <?php do_action('woocommerce_simple_add_to_cart'); ?>
                            </li>

                        </ul>
                        <div class="review">
                            <ul class="tab-review">
                                <li class="tab-btn active" attr=".quick">Quick Overview</li>
                                <li>
                                    <div class="tab-line"></div>
                                </li>
                                <li class="tab-btn" attr=".review">
                                    reviews
                                </li>
                            </ul>
                            <div class="tab-details-sec">
                                <p class="data quick active">
                                    <?= $description ?>

                                </p>
                                <div class="data review">
                                    <?php
                                    $comments = get_comments(array(
                                        'post_id' => $post_id,
                                        'status' => 'approve'
                                    ));
                                  $a =  wp_list_comments(array(
                                        'per_page' => 10,
                                        'reverse_top_level' => false
                                    ), $comments);
                                    ?>
                                </div>

                            </div>
                        </div>
                </div>
            </div>
        </div>


    </section>
<section class="related-products">
    <div class="container-fluid">
        <?php
        // woocommerce_related_products( array(
        //     'posts_per_page' => 4,
        //     'columns'        => 4,
        //     'orderby'        => 'rand'
        // ) );

        // $related_product = custom_related_products($product);
        // echo '<pre>';print_r($related_product);die()
        ?>

         <?php
            $related_product = custom_related_products($product);
          ?>  

        <?php if ($related_product): ?>
         <h2 class="product-name">Related products</h2>
        <?php endif; ?>
         <div class="owl-carousel product-details myproduct owl-theme">
            <?php
            
            if($related_product){
                foreach ($related_product as $key => $product_id) {
                    $post_id = $product_id;
                    $product = new WC_product($post_id);
                    $image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'single-post-thumbnail');
                    $title = $product->get_title();
                    $price = $product->get_price_html();
                    $r_price = (float) $product->get_regular_price();
                    $s_price = (float) $product->get_price();
                    $precision = 1;
                    if($s_price && $r_price){
                        $s_percentage = round( 100 - ( $s_price / $r_price * 100 ), 1 ) . '%';
                    }else{
                        $s_percentage = '0%';
                    }
                    ?>
                    <div class="item" style="height:unset">
                        <div class="product-active">
                            <div class="percentage">
                                <?= $s_percentage ?> <br> Off
                            </div>
                            <div class="p-item-box">
                                <img src="<?php if($image): ?><?= $image[0] ?><?php endif; ?>" alt="">
                            </div>
                            <div class="p-subtitle">
                                <span class="title-highlight"><?= $title ?></span>
                            </div>
                            <ul class="r-p-item">
                                <li class="sbmt">
                                    <div class="price-w"><?=$price ?></div>
                                    <button onclick="window.location.href = '<?php echo site_url() ?>?add-to-cart=<?=$post_id ?>';" class="btn btn-primary">Buy</button>
                                </li>

                            </ul>

                        </div>
                    </div>

                    <?php
                }

            }
            ?>
        </div>


    </div>
</section>
<section class="add">

        <div class="container-fluid">

            <div class="row">

                <div class="col-sm-6">

                    <div class="add-se">

                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/add-sec1.jpg" alt="">

                    </div>

                </div>

                <div class="col-sm-6">

                    <div class="add-se">

                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/add-sec2.jpg" alt="">

                    </div>

                </div>

            </div>

        </div>

    </section>

    <section class="client">

        <div class="container-fluid">

            <div class="owl-carousel client-slider owl-theme">
                <div class="item">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/client/client1.jpg" alt="">
                </div>
                <div class="item">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/client/client2.jpg" alt="">
                </div>
                <div class="item">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/client/client3.jpg" alt="">
                </div>
                <div class="item">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/client/client2.jpg" alt="">
                </div>
                <div class="item">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/client/client1.jpg" alt="">
                </div>
                <div class="item">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/client/client3.jpg" alt="">
                </div>
                <div class="item">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/client/client2.jpg" alt="">
                </div>
            </div>

        </div>

    </section>

    <section class="uline"></section>
<?php get_footer('shop'); ?>

<script>
    jQuery(function () {
        jQuery('.comment-reply-link').hide();
        jQuery('.quantity .screen-reader-text').hide();


        // jQuery('span.onsale').remove();
        // jQuery('.related-products').find('.star-rating').remove();
        // jQuery('.related-products').find('ul').addClass('owl-carousel owl-theme');
        // jQuery('.related-products').find('li').each(function(){
        //     jQuery(this).addClass('item');
        //     var li = jQuery(this).clone();
        //     li.find('a').first().remove();
        //     li.find('.add_to_cart_button').html("Add")

        //     jQuery(this).find('.price').find('del').remove();
        //     var price = jQuery(this).find('.price').find('ins');
        //     var price_content = price.html();
        //     price.remove();

        //     var button = jQuery(this).find('.add_to_cart_button');
        //     button.remove();

        //     var div = document.createElement('div');
        //     div.setAttribute('class','sadeen-product-button');
        //     div.innerHTML = price_content+li.html();
            
        //     jQuery(this).append(div);

        // });

        $('.product-details').owlCarousel({
            loop: true,
            dots: false,
            autoplay: true,
            margin: 10,
            nav: true,
            animateOut: 'fadeOut',
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 4,
                }
            }

        });

        $('.client-slider').owlCarousel({
                loop: true,
                dots: false,
                autoplay: true,
                margin: 10,
                animateOut: 'fadeOut',
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 2,
                        nav: false
                    },
                    600: {
                        items: 4,
                        nav: false
                    },
                    1000: {
                        items: 7,
                        nav: false,
                    }
                }

        });


    });
</script>
