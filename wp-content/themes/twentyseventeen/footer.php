

<?php wp_footer(); ?>
<div class="clearfix"></div>
<footer>

    <div class="container-fluid">


        <ul class="footer-sec">
            <li class="head">
                CAN WE HELP YOU?
            </li>
            <li><a href="#"> 8001 8002 </a></li>
            <li><a href="#">inquiry@sadeen.org </a></li>
            <li><a href="#">+971 4 252 2544 </a></li>



        </ul>

        <ul class="footer-sec">
            <li class="head">
                INFORMATION
            </li>
            <li><a href="#"> Terms & Conditions </a></li>
            <li><a href="#"> Delivery Information </a></li>
            <li><a href="#"> Privacy Policy </a></li>
            <li><a href="#"> Refund Return Policy </a></li>
            <li><a href="#"> Sitemap </a></li>
        </ul>

        <ul class="footer-sec">
            <li class="head">
                CUSTOMER SERVICE
            </li>
            <li><a href="#">  About us </a></li>
            <li><a href="#">Contact Us</a></li>
            <li><a href="#"> FAQ </a></li>
            <li><a href="#"> Create Account </a></li>

        </ul>





        <ul class="footer-sec">
            <li class="head">
                MY ACCOUNT
            </li>
            <li><a href="#"> My Account </a></li>
            <li><a href="#">Order History</a></li>
            <li><a href="#"> My Wishlist </a></li>
            <li><a href="#"> My-cart </a></li>

        </ul>


        <ul class="footer-sec">
            <li class="head">
                Connect With Us
            </li>
            <li><a href="#">Facebook </a></li>
            <li><a href="#"> Twitter</a></li>
            <li><a href="#"> Instagram </a></li>
            <li><a href="#">  Linkedin </a></li>

        </ul>




    </div>
    <div class="footer-bottom">
        <div class="container-fluid">
            <ul>
                <li>
                    Terms and Conditions | Switch to Mobile vertion | Privacy Statement | © 2019 sadeenonline. All rights reserved.
                </li>
                <li>
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/f-logo-png.png" alt="">
                </li>
            </ul>
        </div>

    </div>
</footer>
</body>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="<?php echo get_template_directory_uri() ?>/assets/js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/jquery-cookie.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/owl.carousel.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/jquery.fancybox.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/parallax.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/wow.min.js">
</script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/custom.js"></script>
</html>
