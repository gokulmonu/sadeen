<?php

/**

 * The header for our theme

 *

 * This is the template that displays all of the <head> section and everything up until <div id="content">

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package WordPress

 * @subpackage Twenty_Seventeen

 * @since 1.0

 * @version 1.0

 */



?>

<!DOCTYPE html>

<html <?php language_attributes(); ?> class="no-js no-svg">



<head>

    <!-- Required meta tags -->

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"

        integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"

        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <link

        href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap&subset=cyrillic,cyrillic-ext,latin-ext,vietnamese"

        rel="stylesheet">

    <link rel="stylesheet" type="text/css"

        href="<?php echo get_template_directory_uri() ?>/assets/css/jquery.fancybox.min.css">





    <link rel="icon" href="<?php echo get_template_directory_uri() ?>/assets/images/fav" sizes="16x16" type="image/png">

    <link rel="stylesheet" type="text/css"

        href="<?php echo get_template_directory_uri() ?>/assets/css/slick-theme.css" />

    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/animate.css">

    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/owl.carousel.min.css">

    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/owl.theme.default.min.css">

    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/style.css">





    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/responsive.css">







    <title>sadeen</title>

    <?php wp_head(); ?>
    <style>
    .banner
    {
       height:210px!important; 
    }
    </style>

</head>



<body <?php body_class(); ?>>

    <?php wp_body_open(); ?>

    <header id="main-header">

        <div class="top-section">

            <div class="container-fluid">

                <ul>

                    <li class="contact">

                        <span><a href="#"> <i class="fas fa-phone"> </i></a> <a href="#"><span>  +971 4 252 2544</span> </a> </span>

                        <span><a href="#"> <i class="fas fa-envelope"></i></a> <a href="#"><span>inquiry@sadeen.org</span> </a>

                        </span>

                    </li>

                    <li class="adv">

                        <a href="#">

                            Advertise With Us

                        </a>



                    </li>

                </ul>

            </div>



        </div>

        <div class="logo-section">

            <div class="container-fluid">

                <div class="logo">

                    <a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri() ?>/assets/images/logo.png" alt="">
                    </a>

                </div>

                <div class="right-content">

                    <ul>

                        <li class="search">

                            <div class="input-group">

                                <!-- <input type="text" class="form-control" placeholder="product"

                                    aria-label="Recipient's username" aria-describedby="basic-addon2">

                                <div class="input-group-append">

                                    <span class="input-group-text" id="basic-addon2">Search</span>

                                </div> -->
                                <?php echo do_shortcode("[aws_search_form]"); ?>

                            </div>

                        </li>

                        <li class="ship"><img

                                src="<?php echo get_template_directory_uri() ?>/assets/images/icon/shipment.png" alt="">

                          <span> Track Your Shipment</span> 
                            
                            </li>

                        <li class="lj"> <img src="<?php echo get_template_directory_uri() ?>/assets/images/icon/login.png" alt="">

                            <a href="<?php echo get_home_url(); ?>/my-account/">Sign In </a>

                            <a href="<?php echo get_home_url(); ?>/my-account/?action=register">Join Free</a>

                        </li>

                        <li>
                            <?= do_shortcode('[ti_wishlist_products_counter]');?>
                            <a href="<?php echo wc_get_cart_url(); ?>">
                            <span class="battery">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/icon/battery.png" alt="">
                                <?php
                                    $amount = floatval( preg_replace( '#[^\d.]#', '', WC()->cart->cart_contents_total ) );
                                    
                                ?>
                                <span class="price">
                                    <?=number_format($amount,2) ?>
                                </span>
                            </span>
                            </a>


                        </li>

                    </ul>

                </div>

            </div>



        </div>

    

        





        <div class="menusection">

            <nav class="navbar navbar-expand-lg navbar-light bg-white py-3 shadow-sm">

                <div class="container">

                  

                    <button type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbars"

                        aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler">

                        <span class="navbar-toggler-icon"></span>

                    </button>





                    <div id="navbarContent" class="collapse navbar-collapse">

                        <ul class="navbar-nav mr-auto">





                            <?php



                              $taxonomy     = 'product_cat';

                              $orderby      = 'name';  

                              $show_count   = 0;

                              $pad_counts   = 0;

                              $hierarchical = 1;

                              $title        = '';  
                
                              $empty        = 0;



                              $args = array(
                                     'taxonomy'     => $taxonomy,
                                     'orderby'      => $orderby,
                                     'show_count'   => $show_count,
                                     'pad_counts'   => $pad_counts,
                                     'hierarchical' => $hierarchical,
                                     'title_li'     => $title,
                                     'hide_empty'   => $empty,
                                     'parent' => 0
                              );

                             $all_categories = get_categories($args);
                             $order = array('coffee', 'coffee-machines','grinders','accessories','food-products','brands');
                            //  $array = array(
                            //     array('id' => 7867867, 'title' => 'Some Title'),
                            //     array('id' => 3452342, 'title' => 'Some Title'),
                            //     array('id' => 1231233, 'title' => 'Some Title'),
                            //     array('id' => 5867867, 'title' => 'Some Title'),
                            //  );
                            
                            usort($all_categories, function ($a, $b) use ($order) {
                                $pos_a = array_search($a->slug, $order);
                                $pos_b = array_search($b->slug, $order);
                                return $pos_a - $pos_b;
                            });
                            //echo '<pre>';print_r($all_categories);die();

                             foreach ($all_categories as $cat) {

                                if($cat->category_parent == 0 && trim($cat->name) != "Uncategorized") {

                                    $category_id = $cat->term_id;

                                    ?>



                                        <!-- Level one dropdown -->

                                        <li class="nav-item dropdown">

                                            <a id="dropdownMenu<?=$category_id ?>" href="#" data-toggle="dropdown" aria-haspopup="true"

                                                aria-expanded="false" 

                                                class="nav-link dropdown-toggle"><?=$cat->name ?>

                                            </a>





                                    <?php



                                    $args2 = array(

                                            'taxonomy'     => $taxonomy,

                                            'child_of'     => 0,

                                            'parent'       => $category_id,

                                            'orderby'      => $orderby,

                                            'show_count'   => $show_count,

                                            'pad_counts'   => $pad_counts,

                                            'hierarchical' => $hierarchical,

                                            'title_li'     => $title,

                                            'hide_empty'   => $empty

                                    );

                                    $sub_cats = get_categories( $args2 );

                                    if($sub_cats) {

                                        ?>

                                             <ul aria-labelledby="dropdownMenu<?=$category_id ?>" class="dropdown-menu border-0 shadow">

                                        <?php

                                        foreach($sub_cats as $sub_category) {



                                             



                                                $sub_category_id = $sub_category->term_id; 

                                                 $args2 = array(

                                                        'taxonomy'     => $taxonomy,

                                                        'child_of'     => 0,

                                                        'parent'       => $sub_category_id,

                                                        'orderby'      => $orderby,

                                                        'show_count'   => $show_count,

                                                        'pad_counts'   => $pad_counts,

                                                        'hierarchical' => $hierarchical,

                                                        'title_li'     => $title,

                                                        'hide_empty'   => $empty

                                                );

                                                $inner_cats = get_categories( $args2 );

                                                if($inner_cats){



                                                    ?>

                                                       <li class="dropdown-submenu">

                                                            <a id="dropdownMenu<?=$sub_category_id ?>" href="#" role="button" data-toggle="dropdown"

                                                                aria-haspopup="true" aria-expanded="false"

                                                                class="dropdown-item dropdown-toggle"><?=$sub_category->name ?> <i class="fas fa-caret-right"></i></a>

                                                                <ul aria-labelledby="dropdownMenu<?=$sub_category_id ?>" class="dropdown-menu border-0 shadow"> 



                                                    <?php



                                                    foreach($inner_cats as $inner_c) {

                                                        ?>

                                                            <li><a href="<?=get_term_link($inner_c->slug, 'product_cat') ?>" class="dropdown-item"><?=$inner_c->name ?></a></li>

                                                        <?php

                                                    }



                                                    ?>

                                                        </ul>

                                                        </li>

                                                        <?php

                                                }else{



                                                    ?>

                                                         <li>

                                                            <a href="<?=get_term_link($sub_category->slug, 'product_cat') ?>" class="dropdown-item"><?=$sub_category->name ?></a>

                                                        </li>



                                                    <?php

                                                }





                                           

                                        }

                                        ?>

                                            </ul>

                                        <?php   

                                    }

                                    ?>

                                        </li>

                                    <?php









                                }       

                            }



                            ?>

                                </ul>

                            </li>

                            <!-- End Level one -->

                        </ul>

                    </div>

                </div>

            </nav>



            

        </div>













    </header>

    <?php if (is_page_template( 'template-home.php' ) ): ?>

    <section class="banner large">



        <div class="owl-carousel main-slider owl-theme">



            <div class="item banner1">

                <img src="<?php echo get_template_directory_uri() ?>/assets/images/s1.jpg" class="img-responsive">

                <div class="caption">

                    <div class="container-fluid">

                        <div class="banner-title">

                            <h2>

                                Sales <span> 40%</span> <br> in Bulk Shopping!

                            </h2>

                            <a href="#" class="discover-more">Discover More</a>

                        </div>

                    </div>

                </div>

            </div>

            <div class="item banner2">

                <img src="<?php echo get_template_directory_uri() ?>/assets/images/slider/s3.jpg"

                    class="img-responsive">

                <div class="caption">

                    <div class="banner-title">

                        <h2>

                            Now <span class="red">

                                Kimbo Coffee

                            </span> <br> Even Better In EveryWay

                        </h2>

                        <a href="#" class="discover-more">Discover More</a>

                    </div>



                </div>

            </div>

            <div class="item banner3">

                <img src="<?php echo get_template_directory_uri() ?>/assets/images/slider/s2.jpg"

                    class="img-responsive">

                <div class="caption">

                    <div class="banner-title">

                        <h2>

                            Fresh <span>Food</span> <br> Symbly Delicious

                        </h2>

                        <a href="#" class="discover-more">Discover More</a>

                    </div>



                </div>

            </div>

            <div class="item banner4">

                <img src="<?php echo get_template_directory_uri() ?>/assets/images/slider/s4.jpg"

                    class="img-responsive">

                <div class="caption">

                    <div class="banner-title">

                        <h2>

                            Sales <span> 40%</span> <br> in Bulk Shopping!

                        </h2>

                        <a href="#" class="discover-more">Discover More</a>

                    </div>



                </div>

            </div>

        </div>
</div>
        <?php else: ?>

        <section class="banner" style="height:210px">



            <div class="owl-carousel main-slider owl-theme">



                <div class="item banner1">

                    <div class="container-fluid">

                        <div class="banner-title">

                            <h2>

                                Sales <span> 40%</span> <br> in Bulk Shopping!

                            </h2>

                            <a href="#" class="discover-more">Discover More</a>

                        </div>

                    </div>

                </div>

                <div class="item banner2">

                    <div class="banner-title">

                        <h2>

                            Now <span class="red">

                                Kimbo Coffee

                            </span> <br> Even Better In EveryWay

                        </h2>

                        <a href="#" class="discover-more">Discover More</a>

                    </div>



                </div>

                <div class="item banner3">

                    <div class="banner-title">

                        <h2>

                            Fresh <span>Food</span> <br> Symbly Delicious

                        </h2>

                        <a href="#" class="discover-more">Discover More</a>

                    </div>



                </div>

                <div class="item banner4">

                    <div class="banner-title">

                        <h2>

                            Sales <span> 40%</span> <br> in Bulk Shopping!

                        </h2>

                        <a href="#" class="discover-more">Discover More</a>

                    </div>



                </div>

            </div>





        </section>

        <?php endif ?>



        <!--<section class="bread">-->

        <!--	<div class="container-fluid">-->

        <!--		<ul class="bread-crumb">-->

        <!---->

        <!--			<li><a href="#">  Home</a></li>-->

        <!--			<li><a href="#">Kimbo Coffee Machines</a></li>-->

        <!--			<li class="active"> <a href="#">Capsule automatic machine</a></li>-->

        <!--		</ul>-->

        <!--	</div>-->

        <!--</section>-->
<script type="text/javascript">
    jQuery(function(){
       
    })
</script>