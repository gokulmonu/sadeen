<?php
include_once 'inc/System.php';
$obj = new System();

if (!empty($_POST)) {
    if(($_POST['type'] == "contact"))
        echo $obj->enquiry_submission();
    elseif($_POST['type'] == "quote")
        echo $obj->quote_submission();
    die;
}

$p = (isset($_GET['p'])) ? $_GET['p'] : 'index.php';
$name = $obj->get_name($p);
switch ($name['table_name']){
    case 'page':
        $page = $obj->get_page($name['table_id']);
        $data = $obj->get_data($page);
        $target = $page['type'].'.php';
        break;
    case 'category':
        $page = $obj->get_category($name['table_id']);
        $data = $obj->get_category_data($page);
        $target = 'products.php';
        break;
    case 'category_sub':
        $page = $obj->get_category_sub($name['table_id']);
        $data = $obj->get_category_sub_data($page);
        $target = 'products.php';
        break;
    case 'products':
        $page = $obj->get_product($name['table_id']);
        $data = $obj->get_product_data($page);
        $target = 'product.php';
        break;
    default:
        $data = $obj->get_404();
        $target = '404.php';
}
$targ = 'php/'.$target;
//echo "<pre>";
//print_r($data);
//die;
extract($data);

if (file_exists($targ) && !empty($page)) {
    include $targ;
} else {
   die('Server Not Reachable!');
}
?>