<?php

/*

Template Name: Home 



 * Home Page Template for our theme

 * @subpackage Parallel

 * @since Parallel 1.0

 */

?>



<?php get_header(); $themeoption = 881; ?>

<?php wc_print_notices(); ?>

<section class="deals">

    <div class="container-fluid">

        <div class="row">

            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-center left-box">

                <h4>DEAL OF THE<br> DAY</h4>

                <!-- <p id="demo">Left</p>
 -->
                <p class="margin-bot-0"><a href="<?php echo site_url() ?>/shop">VIEW ALL DEALS</a></p>

            </div>

            <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">

                <div class="owl-carousel owl-theme owl-carousel01">

                   <?php

                   $todaysdeal = get_field("todaysdeal");

                   if($todaysdeal){

                    foreach ($todaysdeal as $key => $product) {

                        $post_id = $product->ID;

                        $product = new WC_product($post_id);

                        $image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'single-post-thumbnail');



                        $title = $product->get_title();

                        $price = $product->get_price_html();

                        ?>

                        <div class="item">
                            <a href="<?=get_permalink($post_id) ?>">
                                <div class="img-box1">
                                <img src="<?php if($image): ?><?= $image[0] ?><?php endif; ?>"/>
                                </div>
                                <p><?=$title ?></p>
                            </a>

                            <p><strong><?=$price ?></strong></p>

                        </div>



                        <?php



                    }





                }





                ?>



            </div>

        </div>

    </div>

</div>

</section>





<!-- <?php echo do_shortcode("[br_products_of_day title='Offer zone' type='slider' count_line='5']"); ?> -->



<section class="offerss">

    

    <div class="offer-side"></div>

    <div class="container-fluid">

        <h2 class="product-name">

            Offer Zone



        </h2>



        <div class="owl-carousel product-details myproduct owl-theme">

            <?php

            $offerzone = get_field("offerzone");

            if($offerzone){

                foreach ($offerzone as $key => $product) {

                    $post_id = $product->ID;

                    $product = new WC_product($post_id);

                    $image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'single-post-thumbnail');



                    $title = $product->get_title();

                    $price = $product->get_price_html();

                    $r_price = (float) $product->get_regular_price();

                    $s_price = (float) $product->get_price();

                    $precision = 1;

                    $s_percentage = round( 100 - ( $s_price / $r_price * 100 ), 1 ) . '%';



                    ?>

                    <div class="item" style="height:unset">

                        <div class="product-active">

                            <div class="percentage">

                                <?= $s_percentage ?> <br> Off

                            </div>

                            <a href="<?=get_permalink($post_id) ?>">
                                <div class="p-item-box">
                                    <img src="<?php if($image): ?><?= $image[0] ?><?php endif; ?>" alt="">
                                </div>
                                <div class="p-subtitle">
                                    <span class="title-highlight"><?= $title ?></span>
                                </div>
                            </a>
                            <ul class="r-p-item">

                                <li class="sbmt">

                                    <div class="price-w"><?=$price ?></div>

                                    <button onclick="window.location.href = '<?php echo site_url() ?>?add-to-cart=<?=$post_id ?>';" class="btn btn-primary">Buy</button>



                                </li>

                            </ul>

                        </div>

                        <div class="offer-line"></div>

                    </div>

                    <?php



                }





            }





            ?>

        </div>

    </div>





    

</section>





<section class="add">

    <div class="container-fluid">

        <div class="row">

            <div class="col-sm-6">

                <div class="add-se">

                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/add-sec1.jpg" alt="">

                </div>

            </div>

            <div class="col-sm-6">

                <div class="add-se">

                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/add-sec2.jpg" alt="">

                </div>

            </div>

        </div>

    </div>

</section>



<section class="most-selling">

    <div class="container-fluid">

        <div class="row">

            <div class="col-sm-6">

                <h2 class="product-name text-center">

                    Most Selling

                </h2>



                <div class="boxxx">

                    <div class="owl-carousel mostselling owl-theme col-xs-12">



                      <?php

                      $args = array(

                        'post_type' => 'product',

                        'meta_key' => 'total_sales',

                        'orderby' => 'meta_value_num',

                        'posts_per_page' => 5,

                    );

                      $loop = new WP_Query( $args );

                      while ( $loop->have_posts() ) : $loop->the_post(); 

                        global $product;

                        $ms_image = wp_get_attachment_image_src(get_post_thumbnail_id($loop->post->ID), 'single-post-thumbnail');



                        $regular_price = (float) $product->get_regular_price();

                        $sale_price = (float) $product->get_price();

                        $precision = 1;

                        if($regular_price && $sale_price){
                             $saving_percentage = round( 100 - ( $sale_price / $regular_price * 100 ), 1 ) . '%';
                         }else{
                            $saving_percentage = "0%";
                         }
                       



                        ?>



                        <div class="item" style="height:unset">

                            <div class="product-active">

                                <div class="percentage" style="color: red">

                                    <?= $saving_percentage ?> <br> Off

                                </div>

                                <a href="<?=get_permalink($loop->post->ID) ?>">
                                    <div class="most-img-box">
                                    <img src="<?php if($ms_image): ?><?= $ms_image[0] ?><?php endif; ?>" alt="">
                                    </div>
                                    <div class="p-subtitle">
                                        <span class="title-highlight"><?php the_title(); ?></span>
                                    </div>
                                </a>





                                <ul class="r-p-item text-center">

                                    <li class="sbmt text-center">

                                        <div class="price-w">

                                            <?php echo $product->get_price_html(); ?>

                                        </div>

                                    </li>

                                    

                                    <button onclick="window.location.href = '<?php echo site_url() ?>?add-to-cart=<?=$loop->post->ID ?>';" class="btn btn-primary">Buy</button>





                                </ul>

                            </div>

                        </div>



                    <?php endwhile; ?>

                    <?php wp_reset_query(); ?>



                </div>

            </div>

        </div>













        <div class="col-sm-6">

            <h2 class="product-name text-center">

                Latest Arrivals

            </h2>

            <div class="boxxx">

                <div class="owl-carousel mostselling owl-theme col-xs-12">





                    <?php

                    $args = array(

                        'post_type' => 'product',

                        'stock' => 1,

                        'posts_per_page' => 5,

                        'orderby' =>'date',

                        'order' => 'DESC' );

                    $loop = new WP_Query( $args );

                    while ( $loop->have_posts() ) : $loop->the_post(); 

                        global $product;

                        $ms_image = wp_get_attachment_image_src(get_post_thumbnail_id($loop->post->ID), 'single-post-thumbnail');



                        $regular_price = (float) $product->get_regular_price();

                        $sale_price = (float) $product->get_price();

                        $precision = 1;

                        $saving_percentage = round( 100 - ( $sale_price / $regular_price * 100 ), 1 ) . '%';



                        ?>



                        <div class="item" style="height:unset">

                            <div class="product-active">

                                <div class="percentage" style="color: red">

                                    <?= $saving_percentage ?> <br> Off

                                </div>
                                <a href="<?=get_permalink($loop->post->ID) ?>">
                                    <div class="latest-image-wrap">
                                    <img src="<?php if($ms_image): ?><?= $ms_image[0] ?><?php endif; ?>" alt="">
                                    </div>
                                
                                    <div class="p-subtitle">
                                        <span class="title-highlight"><?php the_title(); ?></span>
                                    </div>
                                </a>





                                <ul class="r-p-item text-center">

                                    <li class="sbmt text-center">

                                        <div class="price-w">

                                            <?php echo $product->get_price_html(); ?>

                                        </div>

                                    </li>

                                    

                                    <button onclick="window.location.href = '<?php echo site_url() ?>?add-to-cart=<?=$loop->post->ID ?>';" class="btn btn-primary">Buy</button>





                                </ul>

                            </div>

                        </div>



                    <?php endwhile; ?>

                    <?php wp_reset_query(); ?>



                </div>

            </div>

        </div>

    </div>

</div>

</section>



<section class="shop-more">

    <div class="row">



        <?php 

            $specialoffer = get_field("specialoffer");

            $post_id = $specialoffer->ID;

            $product = new WC_product($post_id);

            $image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'single-post-thumbnail');



            $title = $product->get_title();

        ?>



        <div class="col-lg-3 box-l">

            <h2 class="product-name ">

                Special Offer

            </h2>

            <div class="box-in">
                <a href="<?=get_permalink($post_id) ?>">
                    <p><?= $title ?></p>

                    <img src="<?php if($image): ?><?= $image[0] ?><?php endif; ?>" alt="">
                </a>
                <hr>

                <?php echo $product->get_price_html();



                    $sold_count = wh_get_total_sold_by_product_id('2016-05-26', $post_id);

                    $rem_count = $product->get_stock_quantity();

                    if($sold_count && $rem_count){

                        $per = ($sold_count/$rem_count) * 100;

                    }elseif($rem_count){

                        $per = 100;

                    }else{

                        $per = 0;

                    }

                    

                 ?>

                <div class="progress">

                    <div class="progress-bar" style="width:<?=$per ?>%"></div>



                </div> <p><span>Already Sold: <span class="red-color"><?=wh_get_total_sold_by_product_id('2016-05-26', $post_id); ?></span></span><span class="float-right">Awailable : <span class="red-color"><?=$product->get_stock_quantity() ?></span></span></p>



                <p id="demo1"></p>



            </div>

        </div>













        <div class="col-lg-9 box-r">

            <h2 class="product-name ">

                Shop more with sadeen

                <a href="#" class="float-right view-btn">VIEW ALL</a>

            </h2>

            <select id="category_select" class="custom-select">

                

                <?php 

                $args = array(

                         'taxonomy'     => 'product_cat',

                         'orderby'      => 'name',

                         'hide_empty'   => 0,

                         'parent' => 43

                  );

                $categories = get_categories($args);

                foreach($categories as $category) {

                    ?>

                    <option value="<?=$category->slug ?>"><?=$category->name ?></option>

                   <?php

                }

                ?>

             

            </select>





            <div class="tab-content" id="myTabContent">

                <div class="tab-pane fade show active" id="shop1" role="tabpanel" aria-labelledby="shopone">

                    <div class="boxxx">

                        <div class="col-xs-12" id="search_by_category">

                        </div>

                    </div>

                </div>



                

            </div>

        </div>

    </section>
<!--  -->
   <!--  <section class="add">

        <div class="container-fluid">

            <div class="row">

                <div class="col-sm-6">

                    <div class="add-se">

                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/add-sec1.jpg" alt="">

                    </div>

                </div>

                <div class="col-sm-6">

                    <div class="add-se">

                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/add-sec2.jpg" alt="">

                    </div>

                </div>

            </div>

        </div>

    </section> -->
<!--  -->
    <section class="client">

        <div class="container-fluid">

            <div class="owl-carousel client-slider owl-theme">

                <div class="item">

                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/client/client1.jpg" alt="">

                </div>

                <div class="item">

                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/client/client2.jpg" alt="">

                </div>

                <div class="item">

                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/client/client3.jpg" alt="">

                </div>

                <div class="item">

                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/client/client2.jpg" alt="">

                </div>

                <div class="item">

                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/client/client1.jpg" alt="">

                </div>

                <div class="item">

                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/client/client3.jpg" alt="">

                </div>

                <div class="item">

                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/client/client2.jpg" alt="">

                </div>





            </div>

        </div>

    </section>

    <section class="uline"></section>



    <section class="online-store">

        <div class="container-fluid">

            <h2 class="product-name">

                Sadeen-Online Store

            </h2>



            <p>

                Lorem ipsum dolor sit amet, turpis integer pharetra nunc ut, mauris taciti, turpis est. At ut eget elit duis distinctio. Augue quis in porta, eu arcu viverra ut tempor placerat, donec egestas ante nibh integer elementum wisi, et sed nulla vel integer

                porttitor duis. Morbi vel elit dolor, placerat platea tellus arcu proin cursus, voluptate accumsan tristique hendrerit sed luctus. Magna magna vehicula vel blandit, interdum sodales. Mattis cras, dictum odio, donec erat, eu cras platea,

                sem donec curabitur etiam sit. Luctus vulputate. Consequat sit esse sapien suspendisse interdum pharetra, mauris euismod integer habitant ut rutrum dictum, pellentesque ullamcorper magna aenean, turpis sit neque. Quis libero at ultricies,

                nisl vel enim iure pretium.

            </p>

        </div>

    </section>



    <?php get_footer(); ?>

    <script>

        var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";

        jQuery(function () {



            jQuery('#category_select').on('change',function(){

                jQuery('#search_by_category').html("loading please wait...")

                var catId = jQuery(this).val();

                jQuery.ajax({

                    url: ajaxurl,

                    data: {

                        'action': 'get_products_by_category',

                        'category_id' : catId

                    },

                    success:function(data) {

                        var data = jQuery.parseJSON(data);

                        if(data){

                            $("#search_by_category").trigger("destroy.owl.carousel");

                             $("#search_by_category").empty();

                            for(key in data){

                                

                                var item = document.createElement('div');

                                item.setAttribute('class','item');



                                var prod = document.createElement('div');

                                prod.setAttribute('class','product-active');


                                var aTag = document.createElement('a');
                                aTag.setAttribute('href',data[key]['url']);

                                var img = document.createElement('img');
                                img.setAttribute('src',data[key]['image']);
                                
                                aTag.appendChild(img);


                                var titleDiv = document.createElement('div');

                                titleDiv.setAttribute('class','p-subtitle');



                                var spanElem = document.createElement('span');

                                spanElem.setAttribute('class','title-highlight');

                                spanElem.innerHTML = data[key]['title'];

                                titleDiv.appendChild(spanElem);



                                var ulElem = document.createElement('ul');    

                                ulElem.setAttribute('class','r-p-item text-center');  



                                var liElem = document.createElement('li'); 

                                liElem.setAttribute('class','sbmt text-center');





                                var priceDiv = document.createElement('div');

                                priceDiv.setAttribute('class','price-w');

                                priceDiv.innerHTML = data[key]['price'];

                                liElem.appendChild(priceDiv);

                                ulElem.appendChild(liElem);



                                var buttonElem = document.createElement('button');

                                buttonElem.setAttribute('onclick',"window.location.href='<?php echo site_url() ?>?add-to-cart="+data[key]['id']+"'");

                                buttonElem.setAttribute('class','btn btn-primary');

                                buttonElem.innerHTML="Buy";

                                ulElem.appendChild(buttonElem);



                                prod.appendChild(aTag);

                                prod.appendChild(titleDiv);

                                prod.appendChild(ulElem);

                                item.appendChild(prod);

                                jQuery('#search_by_category').append(item);

                            }

                             

                            $("#search_by_category").removeClass('owl-carousel owl-theme owl-controls').addClass('owl-carousel owl-theme owl-controls');

                            jQuery('#search_by_category').owlCarousel(

                                {

                                    loop:true,

                                    margin:10,

                                    nav:true,

                                    navigation : true,

                                    navText: ["PREV","NEXT"],

                                    responsive:{

                                        0:{

                                            items:1

                                        },

                                        600:{

                                            items:3

                                        },

                                        1000:{

                                            items:5

                                        }

                                    }

                                });



                            //jQuery('.owl-nav disabled').show();

                            

                        }else{

                            jQuery('#search_by_category').html("No products found on this category");

                        }

                        

                    },

                    error: function(errorThrown){

                        console.log(errorThrown);

                    }

                });

            });

            jQuery('#category_select').trigger('change');

            $('.owl-carousel01').owlCarousel({

                loop:true,

                margin:10,
dots:false,
                nav:true,

                responsive:{

                    0:{

                        items:1

                    },

                    600:{

                        items:3

                    },

                    1000:{

                        items:5

                    }

                }

            });





            $('.product-details').owlCarousel({

                loop: true,

                dots: false,

                autoplay: true,

                margin: 10,

                nav: true,

                animateOut: 'fadeOut',

            // animateIn: 'fadeOutDown',

            responsiveClass: true,

            // animateIn: 'fadeIn',

            // animateOut: 'fadeOut',

            responsive: {

                0: {

                    items: 1,



                },

                600: {

                    items: 2,



                },

                1000: {

                    items: 4,





                }

            }

        });



            $('.myproduct').owlCarousel({

                loop: true,

                dots: false,

                autoplay: true,

                margin: 10,

                nav: true,

                animateOut: 'fadeOut',

            // animateIn: 'fadeOutDown',

            responsiveClass: true,

            // animateIn: 'fadeIn',

            // animateOut: 'fadeOut',

            responsive: {

                0: {

                    items: 1,



                },

                600: {

                    items: 2,



                },

                1000: {

                    items: 4,





                }

            }

        });



            $('.mostselling').owlCarousel({

                loop: true,

                dots: false,

                autoplay: true,

                margin: 10,

                nav: true,

            // items:3,

            animateOut: 'fadeOut',

            // animateIn: 'fadeOutDown',

            // responsiveClass: true,

            // animateIn: 'fadeIn',

            // animateOut: 'fadeOut',

            responsive: {

                0: {

                    items: 1,



                },

                600: {

                    items: 3



                },

                1000: {

                    items: 3,





                }



            }

        });



            $('.shopmore').owlCarousel({

                loop: true,

                dots: false,

                autoplay: true,

                margin: 10,

                nav: true,

            // items:3,

            animateOut: 'fadeOut',

            // animateIn: 'fadeOutDown',

            // responsiveClass: true,

            // animateIn: 'fadeIn',

            // animateOut: 'fadeOut',

            responsive: {

                0: {

                    items: 1,



                },

                600: {

                    items: 3



                },

                1000: {

                    items: 4,





                }



            }

        });



            $('.client-slider').owlCarousel({

                loop: true,

                dots: false,

                autoplay: true,

                margin: 10,

                animateOut: 'fadeOut',

            // animateIn: 'fadeOutDown',

            responsiveClass: true,

            // animateIn: 'fadeIn',

            // animateOut: 'fadeOut',

            responsive: {

                0: {

                    items: 2,

                    nav: false

                },

                600: {

                    items: 4,

                    nav: false

                },

                1000: {

                    items: 7,

                    nav: false,



                }

            }

        });



        })

    </script>

